import java.util.*;

/**
 * Created by MBAIR on 11/16/16.
 */
public class Model {





    private Map<BudgetCategory, TreeMap<Integer, Double>> spentTable = new HashMap<>();     //String is Name of TimeSpan
    private Map<BudgetCategory, TreeMap<Integer, Double>> budgetTable = new HashMap<>();    //String is CategoryName
    private Map<BudgetCategory, TreeMap<Integer, Double>> deficitTable = new HashMap<>();    //String is CategoryName
    private Map<BudgetCategory, TreeMap<Integer, Double>> deficitTablePropogated = new HashMap<>();



    public Calendar getEarliestDate(BudgetCategory budgetCategory, Calendar defaultDate) {
        TreeMap<Integer, Double> table = spentTable.get(budgetCategory);
        Calendar startDate = budgetCategory.getStartDate();

        //Use default Value
        if(startDate != null) {
            return startDate;
        }

        //If not categorydefault use default provided
        if(table == null) {
                return defaultDate;
        }

        //lowest Value
        return budgetCategory.getTimeSpan().getDate(table.firstKey());
    }

    public void buildBudgetTable(TreeSet<BudgetCategory> budgetCategories) {
        Calendar today = Calendar.getInstance();

        for(BudgetCategory budgetCategory : budgetCategories)        {
            Calendar startDate = getEarliestDate(budgetCategory, today);
            budgetTable.put(budgetCategory, budgetCategory.buildBudget(startDate, today) );
        }
    }

    public void addTransactions(List<Transaction> transactions) {
        for(Transaction transaction : transactions)
        {
            this.addTransaction(transaction);
        }
    }

    public void addTransaction(Transaction transaction) {
        this.addTransaction(transaction.getBudgetCategory(), transaction.getSequenceID(), transaction.getPrice());
    }

    public void addTransaction(BudgetCategory budgetCategory, Integer sequenceID, Double price) {
        if(!spentTable.containsKey(budgetCategory))
        {
            spentTable.put(budgetCategory, new TreeMap<>());
        }
        Map<Integer, Double> account = spentTable.get(budgetCategory);

        if(!account.containsKey(sequenceID)) {
            account.put(sequenceID, price);
        } else {
            account.put(sequenceID, account.get(sequenceID) + price);
        }
    }

    void printModel() {
        printTable(spentTable);
    }

    void printBudgetOutline() {
        printTable(budgetTable);
    }

    void printSurplusTable() {
        printTable(deficitTable);
    }

    void printSurplusTablePropogated() {
        printTable(deficitTablePropogated);
    }


    public void printTable(Map<BudgetCategory, TreeMap<Integer, Double>> table) {
        for (Map.Entry<BudgetCategory, TreeMap<Integer, Double>> entry : table.entrySet()) {
            System.out.println(entry.getKey().getCategoryName());
            for (Map.Entry<Integer, Double> entryPair : entry.getValue().entrySet()) {
                String output = entryPair.getKey().toString() + "    " + entryPair.getValue().toString();
                System.out.println(output);
            }
        }
    }


    void runModel() {

    }

    void buildDeficitSurplusTable() {
        deficitTable.clear();
        deficitTable.putAll(budgetTable);
        for (Map.Entry<BudgetCategory, TreeMap<Integer, Double>> entry : spentTable.entrySet()) {
            TreeMap<Integer, Double>deficitTableForCategory  = deficitTable.get(entry.getKey());
            if(deficitTableForCategory != null) {
                TreeMap<Integer, Double> spentTableForCategory = entry.getValue();
                for (Map.Entry<Integer, Double> sequencIDentry : entry.getValue().entrySet()) {
                    Double BudgetValue = deficitTableForCategory.get(sequencIDentry.getKey());
                    if(BudgetValue != null) {
                        Double SpentValue = sequencIDentry.getValue();
                        Double deficit = BudgetValue - SpentValue;
                        deficitTableForCategory.put(sequencIDentry.getKey(), deficit);
                    } else {
                        System.out.println("No Budget FOr ID : " + sequencIDentry.getKey().toString());
                    }
                }
            } else {
                System.out.println("No Budget For Category" + entry.getKey().getCategoryName());
            }
        }
    }

    void propogateDeficitTable() {
        deficitTablePropogated.clear();
        deficitTablePropogated.putAll(deficitTable);
        for (Map.Entry<BudgetCategory, TreeMap<Integer, Double>> entry : deficitTablePropogated.entrySet()) {
            Double sum = 0.0;
            for (Map.Entry<Integer, Double> sequencIDentry : entry.getValue().entrySet()) {
                Double oldValue = sequencIDentry.getValue();
                Double newValue = oldValue + sum;
                entry.getValue().put(sequencIDentry.getKey(), newValue);
                sum += oldValue;
            }
        }

        }

}
