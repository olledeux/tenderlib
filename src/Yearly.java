import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by MBAIR on 11/16/16.
 */
public class Yearly implements TimeSpan {
    private static String name = "Yearly";

    @Override
    public String getString() {
        return name;
    }

    @Override
    public Integer getSequenceID(Calendar date) {
        return date.get(Calendar.YEAR);
    }

    @Override
    public TreeMap<Integer, Double> buildArray(Calendar startDate, Calendar endDate, Double budgetValue) {
        TreeMap<Integer, Double> exPand = new TreeMap<>();
        Calendar tmp  = (Calendar) startDate.clone();
        Calendar tmpEnd = (Calendar) endDate.clone();
        tmpEnd.add(Calendar.YEAR, 1);
        Integer sequenceID = getSequenceID(tmp);
        Integer endID = getSequenceID(tmpEnd);
        while(sequenceID != endID) {
            exPand.put(sequenceID, budgetValue);
            tmp.add(Calendar.YEAR, 1);
            sequenceID = getSequenceID(tmp);
        }
        return exPand;
    }

    @Override
    public Calendar getDate(Integer sequenceID) {
        Calendar cal = Calendar.getInstance();
        cal.set(sequenceID,0,0);
        return cal;
    }
}
