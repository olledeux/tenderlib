import java.util.Calendar;
import java.util.TreeSet;

/**
 * Created by MBAIR on 11/11/16.
 */
public interface Transaction {
    TreeSet<GroupTag> getTags();
    Double getPrice();
    Calendar getDate();
    String getName();
    BudgetCategory getBudgetCategory();

    void addTag(GroupTag tag);
    void addTags(TreeSet<GroupTag> tags);
    void setBudgetCategory(BudgetCategory budgetCategory);
    void setDate(Calendar date);
    void setPrice(double price);
    void setName(String name);
    void setTags(TreeSet<GroupTag> tags);

    Integer getSequenceID();


}
