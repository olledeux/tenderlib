import java.util.List;
import java.util.TreeSet;

/**
 * Created by MBAIR on 11/11/16.
 */
public interface TenderDB {

    void connectDB();
    List<Transaction> loadAllTransactions();
    TreeSet<BudgetCategory> loadBudgetSchema();
}
