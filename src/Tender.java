import java.util.Calendar;
import java.util.List;
import java.util.TreeSet;

public class Tender {
    public static void main (String[] args)
    {
        TenderDB db = new FakeDB();
        db.connectDB();
        List<Transaction> transactions = db.loadAllTransactions();

        System.out.println("Raw Transactions");
        List<String> purchaseList = TransactionFactory.getStringTransactions(transactions);
        GNC.pringStringArray(purchaseList);



        TreeSet<BudgetCategory> budgetCategorySchema = db.loadBudgetSchema();
        List<String> budgetTable = BudgetFactory.loadTable(budgetCategorySchema);

        System.out.println("\nBudgetCategory Schema");
        GNC.pringStringArray(budgetTable);


        Calendar startDate = Calendar.getInstance();
        startDate.set(2012,1,1);
        Yearly weekly = new Yearly();
        System.out.println("Check " + weekly.getSequenceID(startDate));
        Model model = new Model();
        model.addTransactions(transactions);
        model.buildBudgetTable(budgetCategorySchema);

        System.out.println("");
        model.printModel();

        System.out.println("");
        model.printBudgetOutline();


        System.out.println("");
        model.buildDeficitSurplusTable();
        model.printSurplusTable();


        System.out.println("");
        model.propogateDeficitTable();
        model.printSurplusTablePropogated();
    }

}
