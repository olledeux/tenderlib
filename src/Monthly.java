import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by MBAIR on 11/16/16.
 */
public class Monthly implements TimeSpan {
    private static String name = "Monthly";

    @Override
    public String getString() {
        return name;
    }

    @Override
    public TreeMap<Integer, Double> buildArray(Calendar startDate, Calendar endDate, Double budgetValue) {
        TreeMap<Integer, Double> exPand = new TreeMap<>();
        Calendar tmp  = (Calendar) startDate.clone();
        Calendar tmpEnd = (Calendar) endDate.clone();
        tmpEnd.add(Calendar.MONTH, 1);
        Integer sequenceID = getSequenceID(tmp);
        Integer endID = getSequenceID(tmpEnd);
        while(sequenceID != endID) {
            exPand.put(sequenceID, budgetValue);
            tmp.add(Calendar.MONTH, 1);
            sequenceID = getSequenceID(tmp);
        }
        return exPand;
    }

    @Override
    public Integer getSequenceID(Calendar date) {
        return ((date.get(Calendar.YEAR)) * 100) + (date.get(Calendar.MONTH));
    }

    @Override
    public Calendar getDate(Integer sequenceID) {
        Calendar cal = Calendar.getInstance();
        Integer year = sequenceID / 100;
        Integer month = sequenceID % 100;
        cal.set(year,month,0);
        return cal;
    }
}

