import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by MBAIR on 11/11/16.
 */
public class BudgetCategory implements Comparable<BudgetCategory> {
    private String categoryName;
    private Double budget;  //Allowance,
    private TimeSpan timeSpan;  //RecurringTime,
    private Calendar startDate = null;

    TreeMap<Integer, Double> buildBudget(Calendar startDate, Calendar endDate) {
        TreeMap<Integer, Double> budg = timeSpan.buildArray(startDate, endDate, budget);
        return budg;
    }

    public BudgetCategory(String categoryName, Double budget, TimeSpan timeSpan) {
        this.categoryName = categoryName;
        this.budget = budget;
        this.timeSpan = timeSpan;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public TimeSpan getTimeSpan() {
        return timeSpan;
    }

    public void setTimeSpan(TimeSpan timeSpan) {
        this.timeSpan = timeSpan;
    }

    public Calendar getStartDate() {
        return startDate;
    }

    public void setStartDate(Calendar startDate) {
        this.startDate = startDate;
    }

    @Override
    public int compareTo(BudgetCategory o) {
        return this.categoryName.compareTo(o.getCategoryName());
    }

    @Override
    public String toString() {
        return GNC.printWithSpaces(this.getCategoryName(), 12)
                + GNC.printWithSpaces(getBudget().toString(), 10)
                + GNC.printWithSpaces(this.getTimeSpan().getString(), 10);
    }
}

