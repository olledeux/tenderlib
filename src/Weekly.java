
import java.util.Calendar;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by MBAIR on 11/16/16.
 */
public class Weekly implements TimeSpan {
    private static String name = "Weekly";

    @Override
    public String getString() {
        return name;
    }

    @Override
    public Integer getSequenceID(Calendar date) {
        return ((date.get(Calendar.YEAR)) * 10000) + (date.get(Calendar.MONTH)*100) + date.get(Calendar.WEEK_OF_YEAR);
    }

    @Override
    public TreeMap<Integer, Double> buildArray(Calendar startDate, Calendar endDate, Double budgetValue) {
        TreeMap<Integer, Double> exPand = new TreeMap<>();
        Calendar tmp  = (Calendar) startDate.clone();
        Calendar tmpEnd = (Calendar) endDate.clone();
        tmpEnd.add(Calendar.WEEK_OF_YEAR, 1);
        Integer sequenceID = getSequenceID(tmp);
        Integer endID = getSequenceID(tmpEnd);
        while(sequenceID != endID) {
            exPand.put(sequenceID, budgetValue);
            tmp.add(Calendar.WEEK_OF_YEAR, 1);
            sequenceID = getSequenceID(tmp);
        }
        return exPand;
    }

    @Override
    public Calendar getDate(Integer sequenceID) {
        Calendar cal = Calendar.getInstance();
        Integer year = sequenceID / 10000;
        Integer month = (sequenceID % 10000) / 100;
        Integer week = sequenceID % 100;
        cal.set(year,month,week);
        return cal;
    }
}
