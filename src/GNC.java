import java.util.ArrayList;
import java.util.List;

/**
 * Created by MBAIR on 11/11/16.
 */
public class GNC {
    public static void pringStringArray(List<String> stringArray)
    {
        for(String string : stringArray)
        {
            System.out.println(string);
        }
    }


    public static String printWithSpaces(String string, Integer spaces) {
        String withSpaces = string;
        while(withSpaces.length() < spaces) {
            withSpaces += " ";
        }
        return withSpaces;
    }
}
