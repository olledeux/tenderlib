import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by MBAIR on 11/16/16.
 */
public interface TimeSpan {

    String getString();
    Integer getSequenceID(Calendar date);
    TreeMap<Integer, Double> buildArray(Calendar startDate, Calendar endDate, Double budget);
    Calendar getDate(Integer sequenceID);
}
