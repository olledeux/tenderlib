import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

/**
 * Created by MBAIR on 11/17/16.
 */
public class BudgetFactory {
    static public List<String>  loadTable(TreeSet<BudgetCategory> budgetCategories) {
        List<String> budgetTable = new ArrayList<>();
        for(BudgetCategory budgetCategory : budgetCategories) {
            budgetTable.add(budgetCategory.toString());
        }
        return budgetTable;
    }
}
