import java.util.*;

/**
 * Created by MBAIR on 11/11/16.
 */
public class FakeDB implements TenderDB{
    private ArrayList<Transaction> transactions = new ArrayList<>();
    private TreeSet<BudgetCategory> budgetCategory = new TreeSet<>();
    private BudgetCategory groceryBudgetCategory = new BudgetCategory("Groceries", 100.0, new Yearly());
    private BudgetCategory gasBudgetCategory = new BudgetCategory("Gas", 25.0, new Yearly());

    @Override
    public TreeSet<BudgetCategory> loadBudgetSchema() {
        return budgetCategory;
    }

    @Override
    public void connectDB() {

        Calendar oldDate = Calendar.getInstance();
        oldDate.set(2016,1, 5);
        Calendar dateTwo = Calendar.getInstance();
        dateTwo.set(2016,5, 5);
        Calendar dateThree = Calendar.getInstance();
        dateThree.set(2016,1, 12);
        Calendar OoldDate = Calendar.getInstance();
        OoldDate.set(2015,1, 12);
        Transaction one = new TestTransaction(dateTwo, 4.99, gasBudgetCategory, "Sheetz");
        Transaction two = new TestTransaction(oldDate, 3.88, gasBudgetCategory, "Shell");
        Transaction three = new TestTransaction(dateTwo, 24.99, groceryBudgetCategory, "Safeway");
        Transaction four = new TestTransaction(dateTwo, 10.99, groceryBudgetCategory, "Icecream");
        Transaction five = new TestTransaction(OoldDate, 115.0, groceryBudgetCategory, "Icecream");

        transactions.add(one);
        transactions.add(two);
        transactions.add(three);
        transactions.add(four);
        transactions.add(five);

        budgetCategory.add(gasBudgetCategory);
        budgetCategory.add(groceryBudgetCategory);
    }

    @Override
    public ArrayList<Transaction> loadAllTransactions() {
        return transactions;
    }
}
