import java.util.Calendar;
import java.util.TreeSet;

/**
 * Created by MBAIR on 11/11/16.
 */
public class TestTransaction implements Transaction {

    private TreeSet<GroupTag> tags = new TreeSet<>();
    private Calendar date;
    private Double price = 0.0;
    private BudgetCategory budgetCategory;
    private String name = "Default";

    public TestTransaction(Calendar _date, Double _price, BudgetCategory _category, String _name) {
        this.date = _date;
        this.price = _price;
        this.budgetCategory = _category;
        this.name = _name;
    }


    @Override
    public TreeSet<GroupTag> getTags() {
        return tags;
    }

    @Override
    public void addTag(GroupTag tag) {
        tags.add(tag);

    }

    @Override
    public Double getPrice() {
        return price;
    }

    @Override
    public Calendar getDate() {
        return date;
    }

    @Override
    public String getName() {
        return name;
    }

    public BudgetCategory getBudgetCategory() {
        return budgetCategory;
    }

    @Override
    public Integer getSequenceID() {
        return this.budgetCategory.getTimeSpan().getSequenceID(this.date);
    }

    @Override
    public void addTags(TreeSet<GroupTag> tags) {
        this.tags.addAll(tags);
    }


    public void setBudgetCategory(BudgetCategory category) {
        budgetCategory = category;
    }

    @Override
    public void setDate(Calendar date) {
        this.date = date;
    }

    @Override
    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setTags(TreeSet<GroupTag> tags) {
        this.tags = tags;
    }
}
