import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by MBAIR on 11/11/16.
 */
public class TransactionFactory {

    public static List<String> getStringTransactions(List<Transaction> transactions){

        return transactions.stream()
                .map(t -> TransactionFactory.toFormatString(t))
                .collect(Collectors.toList());
    }


    public static String toFormatString(Transaction transaction) {
        String formattedString;
        formattedString =
                GNC.printWithSpaces(transaction.getName(), 15)
                + GNC.printWithSpaces(transaction.getDate().getTime().toString(), 40)
                + GNC.printWithSpaces(transaction.getPrice().toString(), 10)
                + GNC.printWithSpaces(transaction.getBudgetCategory().getCategoryName(), 15)
                + GNC.printWithSpaces(transaction.getBudgetCategory().getTimeSpan().getString(), 15);
        for(GroupTag tag : transaction.getTags()) {
            formattedString += tag.getTag() +  ",";
        }
        return formattedString;
    }

}
